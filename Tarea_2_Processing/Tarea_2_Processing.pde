//En primer lugar, se establece el tamaño del lienzo: 1000 x 750 pixeles, fondo rosado
void setup(){size(1000,750);
ellipseMode(RADIUS);
background(255,200,200);}
//Se establecen las variables los botones que corresponden a los sensores
//de aceleración (circ_acel) y frenado(circ_fren)
int circ_acel_x=300;
int circ_acel_y=600;
int circ_fren_x=700;
int circ_fren_y=600;
int radio=100;

void draw() {
//círculo de aceleración
float distancia_a=dist(mouseX,mouseY,circ_acel_x,circ_acel_y);
if(distancia_a<radio){fill(0);}else{fill(255);}
ellipse(circ_acel_x,circ_acel_y,radio,radio);
textSize(25);
fill(0,400,600);
text("Aceleración", 250, 725);

//círculo de frenado
float distancia_f=dist(mouseX,mouseY,circ_fren_x,circ_fren_y);
if(distancia_f<radio){fill(0);}else{fill(255);}
ellipse(circ_fren_x,circ_fren_y,radio,radio);
textSize(25);
fill(0,400,600);
text("Frenado", 665, 725);
}
  
 
